#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdbool.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;

    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

void bmp_header_print(struct bmp_header const * bmpHeader );
void bmp_data_print( struct image const * img );
struct bmp_header bmp_header_generate( struct image const * img );
bool is_bmp_header_correct( struct bmp_header const * bmpHeader );

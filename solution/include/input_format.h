#include "bmp.h"
#ifndef UNTITLED2_INPUT_FORMAT_H
#define UNTITLED2_INPUT_FORMAT_H

#endif //UNTITLED2_INPUT_FORMAT_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_BITS,
    READ_FILE_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status bmp_read( struct image * img, FILE * file );
enum write_status bmp_save( struct image img, FILE * file );

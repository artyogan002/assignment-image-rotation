#include <stdint.h>
#include <malloc.h>
#include "util.h"

#ifndef UNTITLED2_IMAGE_H
#define UNTITLED2_IMAGE_H

#endif //UNTITLED2_IMAGE_H

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void pixel_print( struct image const * img, size_t i );
uint8_t get_padding( uint64_t width );
struct image create_image( uint64_t width, uint64_t height );

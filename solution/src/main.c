#include "transformations.h"

int main( int argc, char** argv ) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning

    FILE *file_in = fopen(argv[argc - 2], "rb");
    FILE *file_out = fopen(argv[argc - 1], "wb");

    struct image img = {0};
    bmp_read(&img, file_in);
    struct image imgr = {0};
    image_rotate(&img, &imgr);
    bmp_save(imgr, file_out);

    free(img.data);
    free(imgr.data);

    if (file_in != NULL) {
        fclose(file_in);
    }
    if (file_out != NULL) {
        fclose(file_out);
    }
    return 0;
}

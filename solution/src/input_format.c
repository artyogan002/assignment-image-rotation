#include "input_format.h"

enum read_status bmp_read( struct image * img, FILE * file ) {

    struct bmp_header bmpHeader = {0};
    size_t bmp_header_read_res = 0;

    if ( file != NULL ) {
         bmp_header_read_res = fread(&bmpHeader, sizeof(struct bmp_header), 1, file);
    } else return READ_FILE_ERROR;

    if (!is_bmp_header_correct(&bmpHeader) || bmp_header_read_res != 1) {
        return READ_INVALID_HEADER;
    }

    *img = create_image(bmpHeader.biWidth, bmpHeader.biHeight);

    if( img->width > 0 && img->height > 0) {
        for (size_t i = 0; i < img->width * img->height; i++) {

            size_t res1 = fread(&(img->data[i]), sizeof(struct pixel), 1, file);
            if (res1 != 1) return READ_INVALID_BITS;

            if ((i + 1) % img->width == 0) {
                int res2 = fseek(file, get_padding(img->width), SEEK_CUR);
                if (res2 != 0) return READ_INVALID_BITS;
            }
        }
    } else return READ_INVALID_BITS;

    return READ_OK;
}

enum write_status bmp_save( struct image img, FILE * file ) {

    struct bmp_header bmpHeader = bmp_header_generate(&img);

    if ( file != NULL ) {
        fwrite(&bmpHeader, sizeof(bmpHeader), 1, file);
        if (bmpHeader.biWidth > 0 && bmpHeader.biHeight > 0) {
            for (size_t i = 0; i < bmpHeader.biWidth * bmpHeader.biHeight; i++) {
                fwrite(&img.data[bmpHeader.biWidth * bmpHeader.biHeight - i - 1], sizeof(struct pixel), 1, file);
                if ((i + 1) % bmpHeader.biWidth == 0) {
                    fseek(file, get_padding(bmpHeader.biWidth), SEEK_CUR);
                }
            }

            for (size_t i = 0; i < get_padding(bmpHeader.biWidth); i++) {
                putc(0x00, file);
            }
        }
    } else return WRITE_ERROR;

    return WRITE_OK;
}

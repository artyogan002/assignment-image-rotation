#include "image.h"

void pixel_print( struct image const * img, size_t i ) {
    uint8_t red = img->data[i].r;
    uint8_t green = img->data[i].g;
    uint8_t blue = img->data[i].b;
    printf(" [%d %d %d] ", red, green, blue);
}

uint8_t get_padding( uint64_t width ) {
    return (4 - (width*sizeof(struct pixel) % 4)) % 4;
}

struct image create_image( uint64_t width, uint64_t height ) {
    struct image img;
    img.height = height;
    img.width = width;

    uint64_t line_width = img.width + get_padding(img.width);
    img.data = malloc( sizeof(struct pixel) * img.height * line_width );

    return img;
}

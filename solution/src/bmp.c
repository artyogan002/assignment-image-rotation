#include "../include/bmp.h"

#define TYPE (0x4d42)
#define BIT_COUNT (24)
#define SIZE (40)
#define OFF_BITS (54)

void bmp_header_print(struct bmp_header const * bmpHeader ) {
    printf("bfType: %d \n", bmpHeader->bfType);
    printf("bfileSize: %d \n", bmpHeader->bfileSize);
    printf("bfReserved: %d \n", bmpHeader->bfReserved);
    printf("bOffBits: %d \n", bmpHeader->bOffBits);
    printf("biSize: %d \n", bmpHeader->biSize);
    printf("biWidth: %d \n", bmpHeader->biWidth);
    printf("biHeight: %d \n", bmpHeader->biHeight);
    printf("biPlanes: %d \n", bmpHeader->biPlanes);
    printf("biBitCount: %d \n", bmpHeader->biBitCount);
    printf("biCompression: %d \n", bmpHeader->biCompression);
    printf("biSizeImage: %d \n", bmpHeader->biSizeImage);
    printf("biXPelsPerMeter: %d \n", bmpHeader->biXPelsPerMeter);
    printf("biYPelsPerMeter: %d \n", bmpHeader->biYPelsPerMeter);
    printf("biClrUsed: %d \n", bmpHeader->biClrUsed);
    printf("biClrImportant: %d \n", bmpHeader->biClrImportant);
}
void bmp_data_print( struct image const * img ) {
    const size_t width = img->width;
    const size_t height = img->height;

    for ( size_t i = 0; i < width*height; ++i) {
        pixel_print(img, i);
        if ((i + 1) % width == 0) {
            print_newline();
        }
    }
    print_newline();
}

struct bmp_header bmp_header_generate( struct image const * img ) {
    return (struct bmp_header) {
        .bfType = TYPE,
        .bfileSize = OFF_BITS + img->height * (img->width)*3 + get_padding(img->width),
        .bOffBits = OFF_BITS,
        .biSize = SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biBitCount = BIT_COUNT,
        .biSizeImage = img->height * (img->width)*3 + get_padding(img->width)
    };
}

bool is_bmp_header_correct( struct bmp_header const * bmpHeader ) {
    if (bmpHeader->bfType != TYPE || bmpHeader->biBitCount != BIT_COUNT) {
        return false;
    } else return true;
}

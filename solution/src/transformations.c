#include "transformations.h"

void image_rotate( struct image * img_old, struct image * img_r ) {

    const size_t height_new = img_old->width;
    const size_t width_new = img_old->height;

    *img_r = create_image(width_new, height_new);

    if ( width_new > 0 && height_new > 0 ) {
        for (size_t j = 0; j < height_new; j++) {
            for (size_t i = 1; i <= width_new; i++) {
                img_r->data[i + j * width_new - 1] = img_old->data[i * height_new - j - 1];
            }
        }
    }
}
